sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator"
], function (Controller, Filter, FilterOperator) {
	"use strict";
	return Controller.extend("com.sofka.TodoList.controller.App", {
		onInit: function () {},
		/**
		 *@memberOf com.sofka.TodoList.controller.App
		 */
		addTodo: function (oEvent) {
			var oModel = this.getView().getModel();
			var arrayTodos = oModel.getProperty("/todos").map(function (oTodos) {
				return Object.assign({}, oTodos);
			});
			arrayTodos.push({
				title: oModel.getProperty("/newTodo"),
				completed: false
			});
			oModel.setProperty("/todos", arrayTodos);
			oModel.setProperty("/newTodo", "");
		},
		updateCount: function () {
			var oModel = this.getView().getModel();
			var arrayTodos = oModel.getProperty("/todos") || [];
			var count = arrayTodos.filter(function (oTodo) {
				return oTodo.completed !== true;
			}).length;
			oModel.setProperty("/contadorFaltantes", count);
		},
		/**
		 *@memberOf com.sofka.TodoList.controller.App
		 */
		onFilter: function (oEvent) {
			var arrayFilters = [];
			var filtroSeleccion = oEvent.getParameter("item").getKey();

			switch (filtroSeleccion) {
			case "active":
				arrayFilters.push(new Filter("completed", FilterOperator.EQ, false));
				break;
			case "completed":
				arrayFilters.push(new Filter("completed", FilterOperator.EQ, true));
				break;
			default:
				break;
			}
			var oList = this.byId("list_tasks");
			var oBinding = oList.getBinding("items");
			oBinding.filter(arrayFilters, "todos");
		},
		/**
		 *@memberOf com.sofka.TodoList.controller.App
		 */
		clearCompleted: function () {
			
			var oModel = this.getView().getModel();
			var arrayTodos = oModel.getProperty("/todos").map(function (oTodos) {
				return Object.assign({}, oTodos);
			});
			
			var length = arrayTodos.length;
			
			while(length--){
				if( arrayTodos[length].completed === true ) {
					arrayTodos.splice(length,1);
				}
			}
			
			oModel.setProperty("/todos", arrayTodos);

		}
	});
});